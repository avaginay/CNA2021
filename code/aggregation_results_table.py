
import argparse
import logging
import pathlib
import re
import statistics
import sys
import textwrap

import numpy as np
import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import logger
    import pknhandler
    logger.set_loggers()
    logger = logging.getLogger('logger')

except ImportError:
    print("is it the correct working directory ?")
    # %pwd

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Produce a table giving the number of BNs generated for all systems by all teh identification methods',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument('--benchmakspath',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path where the benchmark path are saved.
                            From this path, there is one file per system
                            ''')
                        )
    parser.add_argument('--pknpath',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path where PKN are saved.
                            From this path, there is one file per system corresponding to the pkn in sif format
                            ''')
                        )
    parser.add_argument('--bnpath',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path where BNs are saved.
                            From this path, there is one folder per system in which there is one folder per identification methods
                            ''')
                        )
    parser.add_argument('--updatescheme',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                            Update sheme with which the table is constructed.
                            /!\ the glob applied to find the coverage files is "*_coverage_stgVSts_{args.updatescheme}.csv"
                            ''')
                        )
    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to where the csv table has to be saved
                        ''')
                        )

    args = parser.parse_args()
    # sys.argv = []
    # args = parser.parse_args(sys.argv)

    print(args)

    print("bim")

    table = dict()

    for systemid in args.bnpath.iterdir():  # TODO : itérer sur SBML list !
        if not systemid.is_dir():
            continue

        print('----------')
        print(systemid)
        table[systemid] = dict()

        # ----------
        # Data about the structure of the system :
        system_realID = re.search("BIOMD..........", str(systemid)).group(
            0)  # TODO : do better !!!
        siffilepath = args.pknpath / f"{system_realID}.sif"
        sifstruct = pknhandler.siffile2sifstruct(siffilepath)
        linkstruct = pknhandler.sifstruct2linksstruct(sifstruct)

        # --- number of nodes in the system :
        table[systemid]["nbnodes"] = len(linkstruct)

        # --- max number of inputs :
        dataPKN_nbinputs = []
        for nodechild in linkstruct:
            print(nodechild)
            dataPKN_nbinputs.append(len(linkstruct[nodechild]))
        table[systemid]["maxnbinputs"] = max(dataPKN_nbinputs)

        # --- average of nodes in the system :
        table[systemid]["average_nbinputs"] = statistics.mean(dataPKN_nbinputs)

        # --- median of nodes in the system :
        table[systemid]["median_nbinputs"] = statistics.median(
            dataPKN_nbinputs)

        # ----------
        # identification done or not ?
        for identification_method in ["reveal", "bestfit", "caspots", "our"]:
            identification_done_path = systemid / \
                identification_method / "identification.done"
            table[systemid][identification_method
                            + "_identification_done"] = identification_done_path.exists()
            # if yes : time of the solving
            if identification_done_path.exists():
                print(identification_done_path)
                # BIOMD..........\.identification-our.benchmark.txt
                benchmark_filepath = args.benchmakspath / \
                    f"{system_realID}.identification-{identification_method}.benchmark.txt"
                df_benchmark = pd.read_csv(
                    benchmark_filepath, index_col=None, header=0, sep='\t')
                table[systemid][identification_method
                                + "_cputime_seconds"] = float(df_benchmark["cpu_time"])
                # conversion from seconds to hours :
                # x = benchmark_df.cpu_time / 60 / 60
                table[systemid][identification_method
                                + "_cputime_hours"] = float(df_benchmark["cpu_time"]) / 60 / 60

        # ----------
        # number of BNs:

        for identification_method in ["reveal", "bestfit"]:
            print(identification_method)

            path = systemid / identification_method / "info-BNs.txt"

            if path.exists():
                infoBN = pd.read_csv(path, sep='\t', header=0)
                table[systemid][
                    identification_method + "_nbBNs_generated"] = infoBN.nbBNs_generated[0]
                table[systemid][
                    identification_method + "_nbBNs_withInfluenceSignsOK"] = infoBN.nbBNs_withInfluenceSignsOK[0]
            else:
                table[systemid][
                    identification_method + "_nbBNs_generated"] = 0
                table[systemid][
                    identification_method + "_nbBNs_withInfluenceSignsOK"] = 0

        for identification_method in ["caspots", "our"]:
            print(identification_method)

            nb_bn = len(
                list((systemid / identification_method).glob("*.bnet")))
            table[systemid][identification_method] = nb_bn

        # ----------
        for identification_method in ["reveal", "bestfit", "caspots", "our"]:
            print(identification_method)

            # one datapoint of proportion of transition retrieved per bnet generated
            datapoint_coverage_proportion = []
            # systemid is the complete path of where the bn of the system are stored
            for coveragefile in (systemid / identification_method).glob(f"*_coverage_stgVSts_{args.updatescheme}.csv"):
                print(coveragefile)
                covtable = pd.read_csv(coveragefile, index_col=0)
                print(covtable)
                datapoint_coverage_proportion.append(
                    covtable[args.updatescheme]["retreived_edges__proportion"])

            if len(datapoint_coverage_proportion) > 0:
                # coverage median (proportion)
                table[systemid][
                    identification_method + "_propcov_median"] = np.median(datapoint_coverage_proportion)
                # coverage mean (proportion)
                table[systemid][
                    identification_method + "_propcov_mean"] = np.mean(datapoint_coverage_proportion)
                # coverage std (proportion)
                table[systemid][
                    identification_method + "_propcov_std"] = np.std(datapoint_coverage_proportion)
                # coverage max (proportion)
                table[systemid][
                    identification_method + "_propcov_max"] = np.max(datapoint_coverage_proportion)
                # coverage min (proportion)
                table[systemid][
                    identification_method + "_propcov_min"] = np.std(datapoint_coverage_proportion)
            else:
                # coverage median (proportion)
                table[systemid][
                    identification_method + "_propcov_median"] = np.nan
                # coverage mean (proportion)
                table[systemid][
                    identification_method + "_propcov_mean"] = np.nan
                # coverage std (proportion)
                table[systemid][
                    identification_method + "_propcov_std"] = np.nan
                # coverage max (proportion)
                table[systemid][
                    identification_method + "_propcov_max"] = np.nan
                # coverage min (proportion)
                table[systemid][
                    identification_method + "_propcov_min"] = np.nan

    df = pd.DataFrame.from_dict(table, orient='index')

    df.to_csv(args.pathout)
    print("the end")
