import argparse
import logging
import os
import sys
import textwrap

import pandas as pd
import yaml

import tqdm

try:
    sys.path.insert(1, 'code/')
    import logger
    import sbmlparser
    logger.set_loggers()
    logger = logging.getLogger('logger.make_taskdescrFiles')

except ImportError:
    print("is it the correct working directory ?")
    # %pwd


parser = argparse.ArgumentParser(
    description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('--list_of_sbmlfiles_to_process',
                    type=str,
                    required=True,
                    help=textwrap.dedent('''\
                    Path to the file storing list of sbml files to process
                    ''')
                    )
parser.add_argument('--nodesTypes',
                    required=True,
                    nargs='+',
                    help=textwrap.dedent('''\
                    List of nodeTypes that are to take into account while determining the influences.
                    TODO : take into account all the nodesTypes defined in the function sbmlparser.getListOfNodes
                    ''')
                    )

parser.add_argument('--odscurationtimes',
                    type=str,
                    required=True,
                    help=textwrap.dedent('''\
                    Path to the ods file storing a table storing simulation time used for the curation.
                    The info of this file ae used to determine the length of the simulation.
                    If several values : it takes the bigger ones,
                    If none, exit on error.
                    /!\ all the processed sbml have to have curation information !
                    ''')
                    )

parser.add_argument('--folder_taskdescr_files',
                    type=str,
                    required=True,
                    help=textwrap.dedent('''\
                    Directory where the task descr files (one per SBML in the given list) should be stored.
                    These task descr files contain information extracted from the SBML files, like :
                    - list of node,
                    - duration of the simulation,
                    - ...
                    If needed, they can be manually edited before running the rest of the pipeline.
                    ''')
                    )

args = parser.parse_args()
# sys.argv = ["--odscurationtimes", "table_curatedBIOMD_time-curation-ts.ods"]
# args = parser.parse_args(sys.argv)

logger.info(f"Setting up the folder where task descr files will be written")
os.makedirs(os.path.dirname(args.folder_taskdescr_files), exist_ok=True)

logger.info(
    f"Reading the file storing times used for the curation: {args.odscurationtimes} ")
curation_df = pd.read_excel(
    args.odscurationtimes,
    index_col=0,  # BIOMDID
    engine="odf")


logger.info("Creating the task descriptor file of each sbml file in the list")
# code for progress bar inspired from : https://stackoverflow.com/a/51693787/9879800
with tqdm.tqdm(
    total=os.path.getsize(args.list_of_sbmlfiles_to_process),
    position=0,
    leave=False,
    desc="Doing"
) as pbar:
    with open(args.list_of_sbmlfiles_to_process, 'r') as fsbmllist:
        for sbmlfilepath in fsbmllist:

            pbar.update(len(sbmlfilepath))
            sbmlfilepath = sbmlfilepath[:-1]  # do not take the last \n

            # data_sbml = OrderedDict()
            data_sbml = dict()
            # ---------------
            # sbml_id
            sbml_id = os.path.splitext(os.path.basename(sbmlfilepath))[0]
            pbar.set_description(f"Doing {sbml_id}")
            data_sbml["sbml_id"] = sbml_id
            # sbml_path
            data_sbml["sbml_path"] = sbmlfilepath
            # time simulation taken from curation_df

            curationinfos = curation_df.loc[[sbml_id], :]
            # The following if discard BIOMD for which the unit is "?"
            # if (curationinfos[["has_all_startstopunit"]] == "yes").any()["has_all_startstopunit"]:
            # Because the has_all_startstopunit was constructed using the following :
            # =IF( AND( NOT (ISBLANK(C2));  NOT (ISBLANK(D2) ); NOT (ISBLANK(E2)); NOT(E2="?") ); "yes"; "no")
            #
            # Use this instead :
            if (curationinfos[["stop"]].notna()).any()["stop"]:
                # Able to find (at least one) information about
                # the length of the simultation used in the ts of the curation
                # (among the potentially several plots that have been done)
                # We use the max value :
                data_sbml["time_simu"] = max(curationinfos["stop"])
            else:
                logger.error(
                    f"Could not find curation information about sbml {sbml_id}. Exiting")
                sys.exit(1)

            # The field 'nodesType' tells which kind of nodes are taken into account.
            # It can be any combinaison of the nodesTypes described the docstring of the function sbmlparser.getListOfNodes
            # SBMLspecies is the union of SBMLspeciesconstant  SBMLspeciesnotconstant.
            # They thus are mutually exclusive.
            # TODO : check for impossible choices
            # assert (set(args.nodesTypes) == {
            #         "SBMLspecies", "SBMLparamsnotconstant"})
            data_sbml['nodesTypes'] = args.nodesTypes

            # Assert the chosen nodesTypes are non overlapping
            # Otherwise this might mess up with the sanitarization of nodes IDs
            # which use the nodeType as prefix. But if a node is in 2 different nodeTypes, wtf ?
            # For example :
            #   SBMLspecies and SBMLparamsnotconstant is ok
            #   But SBMLspecies and SBMLleftsideRules might not be
            #   since SBML species can also be modified by SBML rule and thus be in both list.
            #   Then, the same entity would be split into 2 entities. Then it is the mess. (isn't it ?)
            model = sbmlparser.sbmlfile2libsbmlmodel(sbmlfilepath)

            if not model:
                logger.error(
                    f"Cannot retrive an sbml model for {sbmlfilepath}. Error")
                sys.exit(1)

            assert(sbmlparser.pairwise_disjoint_nodesTypes(
                model, data_sbml['nodesTypes']))

            # For each nodesType taken into account,
            # Retrive a list of SAFE VERSION of nodes id
            map_safe = dict()

            for nodesType in data_sbml['nodesTypes']:

                nodeIDs = sbmlparser.getListOfNodes(
                    model, [nodesType], safe=False)

                for n in nodeIDs:
                    # correspondance safe <-> default
                    map_safe[sbmlparser.get_safeNodeID(n, nodesType)] = n

            data_sbml["nodes_mapsafe"] = map_safe

            # ---------------
            # write it all

            os.makedirs(args.folder_taskdescr_files, exist_ok=True)
            with open(os.path.join(args.folder_taskdescr_files, f"{sbml_id}.yaml"), 'w+') as ftaskdescriptor:
                yaml.dump(data_sbml, ftaskdescriptor)

logger.info("Finish")
