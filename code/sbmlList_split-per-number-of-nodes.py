import argparse
import pathlib
import pdb
import sys
import textwrap

import numpy as np

try:
    sys.path.insert(1, 'code/')
    import sbmlparser


except ImportError:
    print("is it the correct working directory ?")
    # %pwd


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Description', formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--sbmlList',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the file storing list of sbml files to process
                        ''')
                        )
    parser.add_argument('--nodesTypes',
                        type=str,
                        nargs='+',
                        required=True,
                        help=textwrap.dedent('''\
                        List of nodes types used to define the number of nodes of the sbml files
                        ''')
                        )
    parser.add_argument('--bins',
                        required=True,
                        type=int,
                        help=textwrap.dedent('''\
                        Bins size used for the spliting.
                        Example : 5
                        -> from 1 to 5, from 6 to 10, from 11 to 15, ...
                        ''')
                        )
    parser.add_argument('--outfolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path where to save the splitted sbmlList
                        ''')
                        )

    args = parser.parse_args()

    sbml_files = []
    numberofnodes = []
    with open(args.sbmlList, "r") as f:
        for line in f:
            print('----------------')
            # each line is the path of a BIOMD file.
            sbml_filepath = line.strip()
            print(sbml_filepath)
            sbml_files.append(sbml_filepath)
            m = sbmlparser.sbmlfile2libsbmlmodel(sbml_filepath)
            numberofnodes.append(
                len(sbmlparser.getListOfNodes(m, args.nodesTypes))
            )

    sbml_files = np.array(sbml_files)
    numberofnodes = np.array(numberofnodes)

    for bin_min, bin_max in zip(range(1, max(numberofnodes) + args.bins, args.bins), range(args.bins, max(numberofnodes) + args.bins, args.bins)):
        print(bin_min, bin_max)
        w, = np.where(np.logical_and(numberofnodes >=
                                     bin_min, numberofnodes <= bin_max))
        print(sbml_files[w])
        subset = sbml_files[w]
        if len(subset) > 0:
            with open(args.outfolder / f"{args.sbmlList.name}_nodesTypes-{'-'.join(args.nodesTypes)}_nbnodes-{bin_min}-{bin_max}", "w") as fout:
                for e in subset:
                    fout.write(f"{e}\n")
