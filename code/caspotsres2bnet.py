
import argparse
import logging
import pathlib
import sys
import textwrap

import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import pknhandler
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


logger = logging.getLogger('logger.caspotsres2bnet')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Parse the resulting csv of caspots and store the synthetized BNs in the form of bnet files', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--binarizedts',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path to the binarizedts.
                            It is used to assign the correct constant boolean function
                            to the nodes for which no function has been assigned by caspots
                            (due to the fact they were having no parents in the pkn)
                            ''')
                        )
    parser.add_argument('--rescsv',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the csv file outputed by caspots
                        ''')
                        )
    parser.add_argument('--outputfolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the folder where the bnet files have to be stored
                        ''')
                        )

    args = parser.parse_args()
    # sys.argv = ["--rescsv", "experiments/xpname/bn/Athaliana-knowledge/caspots/identification.csv", "--outputfolder", "experiments/xpname/bn/Athaliana-knowledge/caspots"]
    # args = parser.parse_args(sys.argv)

    args
    rescsv = pd.read_csv(args.rescsv)

    binarizedts = tshandler.tsfile2pddf(args.binarizedts)

    for bnidx, bn_csv in rescsv.iterrows():
        print("bnet", bnidx)

        # Define a BN as a dictionary where key is the node name and value the expression of the function
        # node names are taken directly from the binarized ts
        bn_dict = {key: [] for key in binarizedts.columns}

        for colname in rescsv.columns:
            if(bn_csv[colname] == 1):
                # the function encoded in 'colname' is present in the BN :
                node, expression = colname.split("<-")
                expression = expression.replace("+", " & ")
                bn_dict[node].append("(" + expression + ")")

        with open(args.outputfolder / f"{bnidx+1}.bnet", "w") as fout:
            # bnidx+1 for the file name to start at 1 instead of 0 (-> it is consistent with the numerotation of BNs outputed by RBoolNet REVEAL & Best-Fit)
            for node in bn_dict:

                # put the correct constant value if the node is observed constant along all the binarized TS
                # (caspots might have try to explain artefact transitions which are in fact due to the noise)

                if binarizedts[node].nunique() == 1:
                    print(
                        f"the node {node} was actually constant in the binarized ts. The function found by caspots was '{bn_dict[node]}' but replacing with its corrct value")
                    bn_dict[node] = []

                if bn_dict[node] == []:
                    # some components do not have a boolean function assigned in the csv resulting of capots
                    # because they were not considered since they had no parents in the pkn.
                    # or just because despite being considered by caspots, the best boolean function appeared to be a constant.
                    # or because it was observed constant in the binarized ts.
                    # lets assign them to constant with the correct value
                    # -> 0 if a majority of -1 in the binarized ts
                    # -> 1 if a majority of 1 in the binarized ts

                    # code adapted from https://stackoverflow.com/a/55887739
                    bn_dict[node] = binarizedts[node].value_counts().index[0]
                    if bn_dict[node] == -1:
                        bn_dict[node] = 0
                    fout.write(f"{node}, {bn_dict[node]} \n")

                else:
                    fout.write(f"{node}, {' | '.join(bn_dict[node])} \n")
