import os
import subprocess

import networkx as nx
import PyBoolNet


def stg2image(STG, FnameIMAGE, LayoutEngine="fdp", Silent=False):
    """
    Creates an image file from a state transition graph using :ref:`installation_graphviz` and the *LayoutEngine*.
    Use ``dot -T?`` to find out which output formats are supported on your installation.
    **arguments**:
        * *STG*: state transition graph
        * *FnameIMAGE* (str): name of output file
        * *LayoutEngine*: one of "dot", "neato", "fdp", "sfdp", "circo", "twopi"
        * *Silent* (bool): print infos to screen
    **example**::
          >>> stg2image(stg, "mapk_stg.pdf")
          >>> stg2image(stg, "mapk_stg.jpg", "neato")
          >>> stg2image(stg, "mapk_stg.svg", "dot")
    """

    print(STG.__class__)
    if STG.__class__ == nx.DiGraph().__class__:
        # Let PyBoolNet plot the STG :
        print("STG has been found to be a simple DiGraph")
        print("hence run PyBoolNet.Utility.DiGraphs.digraph2image")
        PyBoolNet.Utility.DiGraphs.digraph2image(
            STG, FnameIMAGE, LayoutEngine, Silent)
    elif STG.__class__ == nx.MultiDiGraph().__class__:
        # custom
        print("STG has been found to be a MultiDiGraph")
        print("hence run custom digraph2image function")
        multidigraph2image(STG, FnameIMAGE, LayoutEngine, Silent)


def multidigraph2image(MultiDiGraph, FnameIMAGE, LayoutEngine, Silent):
    """
    Inspired from PyBoolNet.Utility.DiGraphs.digraph2image

    Creates an image file from a *MultiDiGraph* file using the Graphviz layout *LayoutEngine*.
    The output format is detected automatically.
    Use e.g. ``dot -T?`` to find out which output formats are supported on your installation.
    **arguments**:
        * *FnameDOT*: name of input *dot* file
        * *LayoutEngine*: one of "dot", "neato", "fdp", "sfdp", "circo", "twopi"
        * *FnameIMAGE*: name of output file
    **returns**:
        * *None*
    **example**::
          >>> dot2image("mapk.dot", "mapk.pdf")
    """

    LAYOUT_ENGINES = PyBoolNet.Utility.DiGraphs.LAYOUT_ENGINES

    filetype = FnameIMAGE.split('.')[-1]

    cmd = [LAYOUT_ENGINES[LayoutEngine], "-T" + filetype, "-o", FnameIMAGE]
    dotfile = multidigraph2dot(MultiDiGraph, FnameDOT=None)
    # custom multidigraph2dot instead of PyBoolNet.Utility.DiGraphs.digraph2dot

    proc = subprocess.Popen(cmd, stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate(input=dotfile.encode())
    proc.stdin.close()

    if not (proc.returncode == 0) or not os.path.exists(FnameIMAGE):
        print(dotfile)
        print(out)
        print(err)
        print('dot did not respond with return code 0')
        print('command: %s' % ' '.join(cmd))
        raise Exception

    if not Silent:
        print("created %s" % FnameIMAGE)


def multidigraph2dot(MultiDiGraph, FnameDOT=None):
    """
    Inspired from PyBoolNet.Utility.DiGraphs.digraph2dot

    Generates a *dot* file from *MultiDiGraph* and saves it as *FnameDOT* or returns dot file as a string.
    **arguments**:
        * *MultiDiGraph*: networkx.MultiDiGraph
        * *FnameDOT* (str): name of *dot* file or *None*
    **returns**:
        * *FileDOT* (str): file as string if not *FnameDOT==None*, otherwise it returns *None*
    **example**::
          >>> multidigraph2dot(digraph, "digraph.dot")
          >>> multidigraph2dot(digraph)
    """

    if MultiDiGraph.order() == 0:
        print("DiGraph has no nodes.")
        if FnameDOT != None:
            print("%s was not created." % FnameDot)
        return

    if not type(list(MultiDiGraph.nodes())[0]) == str:
        MultiDiGraph = networkx.relabel_nodes(
            MultiDiGraph, mapping=lambda x: str(x))

    lines = ['digraph {']

    # /!\ custom multidigraph2dotlines instead of PyBoolNet.Utility.DiGraphs.digraph2dotlines
    lines += multidigraph2dotlines(MultiDiGraph)
    lines += ['}']

    if FnameDOT == None:
        return '\n'.join(lines)

    with open(FnameDOT, 'w') as f:
        f.writelines('\n'.join(lines))
    print("created %s" % FnameDOT)


def multidigraph2dotlines(MultiDiGraph, Indent=1):
    """
    Inspired from PyBoolNet.Utility.DiGraphs.digraph2dotlines

    Basic function to create *dot* lines from a *networkx.DiGraph*.
    **arguments**:
        * *MultiDiGraph* (*networkx.MultiDiGraph*)
    **returns**:
        * list of *dot* lines
    """

    space = Indent * "  "
    lines = []

    # ----------
    # fix cluster_ids for subgraphs

    # MultiDiGraph.graph returns attributes applied on the Graph
    #
    if "subgraphs" in MultiDiGraph.graph:
        for ID, subgraph in enumerate(MultiDiGraph.graph["subgraphs"]):
            subgraph.graph["cluster_id"] = ID

            if not "label" in subgraph.graph:
                subgraph.graph["label"] = ""
                # bugfix for inherited labels
                # see: http://www.graphviz.org/content/cluster-and-graph-labels-bug

    hit = False
    # node and edge defaults first
    for key in ["node", "edge"]:
        if key in MultiDiGraph.graph:
            value = MultiDiGraph.graph[key]
            attr = ', '.join(['%s="%s"' % (str(x), str(y))
                              for x, y in value.items()])
            if attr:
                lines += [space + '%s [%s];' % (key, attr)]
            hit = False

    # general graph attributes
    for key, value in sorted(MultiDiGraph.graph.items()):
        # ignore because treated elsewhere
        key = key.strip()
        if key.lower() in ["subgraphs", "cluster_id", "node", "edge"]:
            continue

        # handle for keys that contain "{"
        # used for graph attributes of the from {rank = same; B; D; Y;}
        if "{" in key:
            lines += [space + key]

        # handle for labels, i.e., the quotation mark problem
        elif key == "label":
            value = value.strip()
            if value.startswith("<"):
                lines += [space + 'label = %s;' % value]
            elif value == "":
                lines += [space + 'label = "";']
                # bugfix for inherited labels
                # see: http://www.graphviz.org/content/cluster-and-graph-labels-bug

            elif value:
                lines += [space + 'label = "%s"' % value.strip()]
                #lines+= [space+'label=<<B>%s</B>>;'%value.replace("&","&amp;")]

        # everything else is just passed on:
        else:
            lines += [space + '%s = "%s";' % (key, value)]

        hit = True

    if hit:
        lines += ['']

    # handle for subgraphs
    if "subgraphs" in MultiDiGraph.graph:
        value = MultiDiGraph.graph["subgraphs"]
        if value:
            tree = subgraphs2tree(value)
            roots = [x for x in tree.nodes() if not list(tree.predecessors(x))]
            for root in roots:
                subnodes = list(networkx.descendants(tree, root)) + [root]
                subtree = tree.subgraph(subnodes)
                lines += tree2dotlines(subtree, Indent)

            lines += ['']

    # node attributes
    for node, attr in sorted(MultiDiGraph.nodes(data=True)):

        if attr:
            values = []
            for k, v in attr.items():

                # html style label attribute
                if str(v).startswith("<"):
                    values += ['%s=%s' % (k, v)]

                # normal attribute
                else:
                    values += ['%s="%s"' % (k, v)]

            values = ', '.join(values)
            lines += [space + '"%s" [%s];' % (node, values)]
        else:
            lines += [space + '"%s";' % node]

    if MultiDiGraph.order() > 0 and MultiDiGraph.size() > 0:
        lines += ['']

    # edge attributes
    # /!\ here is a change compared to the PyBoolNet implementation for DiGraph:
    # we can not access sorted(MultiDiGraph.edges(data=True))
    for source, target, attr in MultiDiGraph.edges(data=True):
        # The edge attribute returned in 3-tuple (u, v, ddict[data]).
        # If True, return edge attribute dict in 3-tuple (u, v, ddict).
        # If False, return 2-tuple (u, v).

        # sign is reserved for interaction signs
        attr = dict([x for x in attr.items() if not x[0] == "sign"])

        if attr:
            values = []
            for k, v in attr.items():

                # html style label attribute
                if str(v).startswith("<"):
                    values += ['%s=%s' % (k, v)]

                # normal attribute
                else:
                    values += ['%s="%s"' % (k, v)]

            values = ', '.join(values)
            lines += [space + '"%s" -> "%s" [%s];' % (source, target, values)]

        else:

            # for edges without attributes
            lines += [space + '"%s" -> "%s";' % (source, target)]

    if MultiDiGraph.size() > 0:
        lines += ['']

    return lines
