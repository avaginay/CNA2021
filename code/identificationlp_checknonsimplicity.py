# from identificationlp_checksimplicity import
# clyngorAnswer2answerDict, answerDict2sympystring, clyngorAnswer2sympy


"""are all the answear minimal DNF ?"""


import imp
import os

import clyngor

from sympy import *
from sympy.logic.boolalg import to_dnf

os.getcwd()


python_code_file = open(
    "code/preview-lattice_overingeeeering.py")
bn2lattice = imp.load_module(
    'python_code', python_code_file, './python-code', ('.py', 'U', 1))


answers = clyngor.solve(
    'code/ASPencoding_version20200326/identify-impossible.lp')
answers = list(answers)
len(answers)


count_minimal = 0
for answer_idx, answer in enumerate(answers):
    print("------", answer_idx)
    answerSympy = clyngorAnswer2sympy(answer)
    print(answerSympy)
    answer_minDNF = to_dnf(answerSympy, simplify=True)
    if answer_minDNF == answerSympy:
        print("problem")
        print(f"{answer}")
        print(f"answer = {answerSympy}")
        print(f"min DNF = {answer_minDNF}")
        count_minimal += 1
        break
