
import argparse
import pathlib
import sys
import textwrap

import pandas as pd

try:
    sys.path.insert(1, 'code/')
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Parse a curationTable.ods file to extract the fileList having a given property',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--sbmlList',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the file storing list of sbml files to process
                        ''')
                        )
    parser.add_argument('--curationTable',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to the ods file storing a table storing simulation time used for the curation.
                        ''')
                        )

    parser.add_argument('--outputsbmlList',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path where to store the sbmlList listing the sbml respecting the property
                        ''')
                        )

    parser.add_argument('--property',
                        type=str,
                        choices=("sbmlwithcurationts",
                                 "sbmlwithcurationts_complete"),
                        required=True,
                        help=textwrap.dedent('''\
                        property
                        - sbmlwithcurationts : the sbml files having their 'type' column equal to 'ts'
                        - sbmlwithcurationts_complete : the sbml files having their column 'has_all_startstopunit'  equal to 'yes'
                        ''')
                        )

    args = parser.parse_args()
    # sys.argv = ["--sbmlList", "", "--curationTable", "table_curatedBIOMD_time-curation-ts.ods"]
    # args = parser.parse_args(sys.argv)

    curation_df = pd.read_excel(
        args.curationTable,
        index_col=0,  # BIOMDID
        engine="odf")

    with open(args.outputsbmlList, "w") as f_out, open(args.sbmlList, 'r') as f_in:
        for sbml_path in f_in:
            sbml_path = pathlib.Path(sbml_path.strip())

            # extract BIOMOID
            sbml_id = sbml_path.stem

            curationinfos = curation_df.loc[[sbml_id], :]
            if (args.property == 'sbmlwithcurationts'
                    and "ts" in curationinfos["type"].values
                    ):
                f_out.write(f"{sbml_path}\n")
            elif (args.property == 'sbmlwithcurationts_complete'
                  and "yes" in curationinfos["has_all_startstopunit"].values
                  ):
                f_out.write(f"{sbml_path}\n")
