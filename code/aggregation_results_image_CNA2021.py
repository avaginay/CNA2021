
import argparse
import pathlib
import textwrap

import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import numpy as np
import pandas as pd
import scipy
import seaborn as sns
from matplotlib.lines import Line2D


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Produce a table giving the number of BNs generated for all systems by all teh identification methods',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--aggregation_results_table',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                            Path to the file which aggregate the results
                            ''')
                        )
    parser.add_argument('--pattern',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        The pattern that will be greped in the column 'system' of the aggragation table.
                        All the lines which have this pattern will be used to plot the figure.
                        ''')
                        )

    parser.add_argument('--jitter_x',
                        type=float,
                        required=True,
                        help=textwrap.dedent('''\
                        Jitter to apply on the x axis
                        ''')
                        )
    parser.add_argument('--jitter_y',
                        type=float,
                        required=True,
                        help=textwrap.dedent('''\
                        Jitter to apply on the y axis
                        ''')
                        )
    parser.add_argument('--hists',
                        type=str2bool,
                        required=True,
                        help=textwrap.dedent('''\
                        Whether adding the histograms around the scatter plots
                        ''')
                        )
    parser.add_argument('--identificationmethods',
                        nargs='+',
                        required=True,
                        help=textwrap.dedent('''\
                        List of identification taken into account.
                        ''')
                        )
    parser.add_argument('--plotX',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        What to plot on y abscisse : either "nbnodes", "maxnbinputs", "average_nbinputs", "median_nbinputs"
                        ''')
                        )
    parser.add_argument('--plotY',
                        type=str,
                        required=True,
                        help=textwrap.dedent('''\
                        What to plot on y abscisse : either "median" or "max"
                        ''')
                        )
    # TODO : Allow plotX to be only median or max
    parser.add_argument('--pathout',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Path to where the aggegation figure for the fiven pattern
                        ''')
                        )

    # Fixing random state for reproducibility
    np.random.seed(42)

    args = parser.parse_args()
    # sys.argv = []
    # args = parser.parse_args(sys.argv)

    print(args)
    print(args.pattern)

    aggregation_table = pd.read_csv(
        args.aggregation_results_table, index_col=0)

    considered_rows = aggregation_table[
        aggregation_table.index.str.contains(args.pattern)]

    # TODO do not consider rows for which the identification where not done :

    # scatter plot with histograms :
    # https://matplotlib.org/3.2.1/gallery/lines_bars_and_markers/scatter_hist.html

    # define figure dimention
    fig_width = 8
    fig_height = 4
    # , constrained_layout=True)
    fig = plt.figure(figsize=(fig_width, fig_height))
    # fig = plt.figure()

    fig.tight_layout()

    if args.hists:
        # Add a gridspec with 2 rows and two columns and a ratio of 2 to 7 between
        # the size of the marginal axes and the main axes in both directions.
        # Also adjust the subplot parameters for a square plot.
        gs = fig.add_gridspec(2, 2,
                              width_ratios=(2, 6),
                              height_ratios=(3, 6),
                              # késoko :
                              left=0, right=0.9, bottom=0, top=0.9,
                              wspace=0.1, hspace=0.1
                              )

        ax_scatter = fig.add_subplot(gs[1, 1])
        ax_scatter.set_xlabel("max # parents", fontweight="bold")
        if args.plotY == "median":
            ylabel = "coverage median"
        else:
            ylabel = args.plotY
        ax_scatter.set_ylabel(ylabel, fontweight="bold")
        ax_scatter.xaxis.set_label_position('top')
        ax_scatter.xaxis.set_ticks_position('top')
        ax_scatter.yaxis.set_label_position('left')
        ax_scatter.yaxis.set_ticks_position('left')

        ax_scatter.hlines(1, 0.3, 10.3, colors="green", alpha=0.5)

        # ==========
        # # histogram on the right :
        # ax_histy = fig.add_subplot(gs[0, 1], sharex=ax_scatter)
        # ax_histy.set_xlabel(args.plotX, fontweight="bold")
        # ax_histy.set_ylabel("     Probability density")
        # plt.setp(ax_histy.get_xticklabels(), visible=False)
        # ax_histy.xaxis.set_label_position('top')
        # ax_histy.xaxis.set_ticks_position('bottom')
        # ax_histy.yaxis.set_label_position('left')
        # ax_histy.yaxis.set_ticks_position('left')
        # # Make the tik label invisible
        # plt.setp(ax_histy.get_xticklabels(), visible=False)
        # # ax_histy.tick_params(axis='x', colors='white')

        # ==========
        # # Histogram on the left :
        # ax_histx = fig.add_subplot(gs[1, 0], sharey=ax_scatter)
        # # labels are inverted since the plot will be in oposite direction
        # ax_histx.set_xlabel("Probability density     ")
        # ax_histx.set_ylabel(args.plotY, fontweight="bold")
        # ax_histx.xaxis.set_label_position('top')
        # ax_histx.xaxis.set_ticks_position('top')
        # ax_histx.yaxis.set_label_position('left')
        # ax_histx.yaxis.set_ticks_position('right')
        # ax_histx.tick_params(axis='y', labelrotation=90)
        # plt.setp(ax_histx.get_yticklabels(), visible=False)
        # # empty_string_labels = ['']*len(labels)
        # # ax_histx.set_yticklabels([''])
        # # ax_histx.tick_params(
        # #     axis='both',
        # #     # which='major',
        # #     # labelsize=10,
        # #     labelbottom=False,
        # #     bottom=True,
        # #     top=True,
        # #     labeltop=False
        # # )

        # no labels
        # ax_histy.tick_params(axis="x", labelbottom=False)
        # ax_histx.tick_params(axis="y", labelleft=False)

    else:
        ax_scatter = fig.add_subplot(111)
        # 111 = "1x1 grid, first subplot"

    title = fig.suptitle(args.pattern)

    ax_scatter.xaxis.set_ticks_position('top')
    ax_scatter.set_xlim(0.7, 10.3)  # -> automatique xlim
    # ax_scatter.set_ylim(0, 65)
    ax_scatter.xaxis.set_ticks(
        np.arange(1, 11, 1))

    # Test to put away the points concerning xp which returned no BNs at all:
    # ax_scatter.set_xlim(-0.6, 1.1)
    #
    # # the - 1 is for method having return no BNs
    # def update_ticks(x, pos):
    #     if x == -0.5:
    #         return 'no BNs'
    #     if x < 0:
    #         return ''
    #     else:
    #         return x
    # ax_scatter.xaxis.set_major_formatter(mticker.FuncFormatter(update_ticks))

    ax_scatter.yaxis.set_ticks_position('left')
    ax_scatter.set_ylim(-0.05, 1.05)
    ax_scatter.yaxis.set_ticks(np.arange(0, 1.1, 0.25))
    ax_scatter.tick_params(axis='y', labelrotation=90)

    # Hide the right and bottom spines
    # ax_scatter.spines['right'].set_visible(False)
    # ax_scatter.spines['bottom'].set_visible(False)

    INFOPLOT = {
        # "reveal": ["REVEAL", "black", "X", ":"],
        # "bestfit": ["Best-Fit", "darkgreen", "s", "-."],
        # "caspots": ["caspot-TS", "darkblue", "v", "--"],
        "our": ["ASKeD-BN", "black", "o", "-"]  # red in CMSB
    }

    # -----
    # X coords (idenpendant of the identification method)
    # "maxnbinputs", "average_nbinput", "median_nbinputs"
    x_coord = considered_rows[args.plotX]
    # -----

    for identificationmethod in args.identificationmethods:

        identification_method = identificationmethod
        color = INFOPLOT[identificationmethod][1]
        shape = INFOPLOT[identificationmethod][2]
        linestyle = INFOPLOT[identificationmethod][3]

        # -----
        # Y coords :
        if args.plotY == "median":
            # propcov_median
            y_coord = considered_rows[identification_method
                                      + "_propcov_median"]

        elif args.plotY == "max":
            # propcov_max
            y_coord = considered_rows[identification_method
                                      + "_propcov_max"]
            # -----

        # TODO : decide with an argument
        # choose what to do if no BN return was return
        #   - put 0
        #   - put np.nan (to not draw them...)
        #   - median is -0.5 (to put it away) :
        if identification_method in ["reveal", "bestfit"]:
            suffix_nbBNs = "_nbBNs_withInfluenceSignsOK"
        else:
            suffix_nbBNs = ""
        x_coord.loc[
            considered_rows[identification_method + suffix_nbBNs] == 0] = np.nan  # 0
        y_coord.loc[
            considered_rows[identification_method + suffix_nbBNs] == 0] = np.nan  # 0

        jittered_x = x_coord + args.jitter_x * \
            np.random.rand(len(x_coord)) - (args.jitter_x / 2)
        jittered_y = y_coord + args.jitter_y * \
            np.random.rand(len(y_coord)) - (args.jitter_y / 2)

        print(identification_method)
        # print(list(zip(jittered_x, jittered_y)))

        labels_fix = {
            "reveal": "REVEAL",
            "bestfit": "Best-Fit",
            "caspots": "caspo-TS",
            "our": "ASKeD-BN"
        }

        colors = ['black' if var == 0 else "red" for var in considered_rows[
            identification_method + "_propcov_std"]]
        ax_scatter.scatter(jittered_x, jittered_y,
                           s=40,
                           # c=colors,
                           facecolor="none",  # fill color
                           edgecolor=colors,  # edge of the shape
                           lw=0.7,  # linewidth
                           marker=shape,
                           label=labels_fix[identification_method],
                           alpha=0.8  # 0.2
                           )
        # ax_scatter.plot(np.unique(x_coord), np.poly1d(np.polyfit(
        #     x_coord, y_coord, 1))(np.unique(x_coord)), color='yellow')
        # error bars :
        # ax_scatter.errorbar(jittered_x, jittered_y,
        #                     yerr=considered_rows[
        #                         identification_method + "_propcov_std"],
        #                     fmt='none',  # None -> no point are shown, only error bars# .k
        #                     ecolor='red'  # color of he errorbars
        #                     )

        # if args.hists:
        #     # density_x = sns.kdeplot(
        #     #     x_coord,
        #     #     ax=ax_histy,
        #     #     # bw = 0.5 , # Smoothing parameter.. deprecated
        #     #     color=color,
        #     #     linewidth=1,
        #     #     label="",
        #     #     linestyle=linestyle
        #     # )
        #
        #     # density_y = sns.kdeplot(
        #     #     y_coord,
        #     #     ax=ax_histx,
        #     #     # bw = 0.5 , # Smoothing parameter.. deprecated
        #     #     color=color,
        #     #     linewidth=1,
        #     #     vertical=True,
        #     #     label="",
        #     #     linestyle=linestyle
        #     # )
        #
        #     # if args.hists:
        #     #     import scipy.stats
        #     #
        #     #     x_coord_density =  scipy.stats.gaussian_kde(x_coord)
        #     #
        #
        #     # if args.hists:
        #     #
        #     #     # determine nice limits by hand:
        #     #     #binwidth = 0.25
        #     #     #xymax = max(np.max(np.abs(x_coord)), np.max(np.abs(y_coord)))
        #     #     #lim = (int(xymax / binwidth) + 1) * binwidth
        #     #
        #     #     #bins = np.arange(-lim, lim + binwidth, binwidth)
        #     #     ax_histy.hist(
        #     #         x_coord,
        #     #         density=False,
        #     #         # About density :
        #     #         # If True, the result is the value of the probability density function at the bin,
        #     #         # normalized such that the integral over the range is 1.
        #     #         # Note that the sum of the histogram values will not be equal to 1 unless bins of unity width are chosen;
        #     #         # it is not a probability mass function.
        #     #         # ---
        #     #         # https://github.com/matplotlib/matplotlib/issues/10398/#issuecomment-366021979
        #     #         # for the bars to sum to one (independent of the bin widths),
        #     #         # given density = False and weigths = np.ones(len(y_coord)) / len(y_coord)
        #     #         weights=np.ones(len(x_coord)) / len(x_coord),
        #     #         histtype='step',
        #     #         # bins=bins,
        #     #         color=color,
        #     #         edgecolor=color,
        #     #         fill=False,
        #     #         alpha=0.8
        #     #     )
        #     #     ax_histx.hist(
        #     #         y_coord,
        #     #         density=False,
        #     #         weights=np.ones(len(y_coord)) / len(y_coord),
        #     #         histtype='step',
        #     #         # bins=bins,
        #     #         color=color,
        #     #         edgecolor=color,
        #     #         fill=False,
        #     #         alpha=0.8,
        #     #         orientation='horizontal'
        #     #     )
        #
        #     # legend outside the figure :
        #     # lgd = plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
        #     # lgd = plt.legend(bbox_to_anchor=(7, 7), loc='upper left')
        #     # lgd = ax_scatter.legend(bbox_to_anchor=(1.05, -0.05), bbox_transform=ax_scatter.transAxes,
        #     #                         loc='upper left')

    handlers_shapes, labels = ax_scatter.get_legend_handles_labels()
    # handles_lines, _ = ax_histy.get_legend_handles_labels()
    handlers_lines = []
    for identificationmethod in args.identificationmethods:
        handlers_lines.append(Line2D(
            [0], [0], color=INFOPLOT[identificationmethod][1], linewidth=1, linestyle=INFOPLOT[identificationmethod][3]))

    lgd = ax_scatter.legend(
        handles=zip(handlers_shapes, handlers_lines),
        labels=labels,
        scatterpoints=1,
        bbox_to_anchor=(-0.40, 0.9),
        # bbox_transform=ax_histy.transAxes,
        loc='upper left'
    )

    ## fig.legend(handles, labels, loc='upper center')
    ##
    # lgd = ax_scatter.figlegend(bbox_to_anchor=(-0.40, 0.9), bbox_transform=ax_histy.transAxes,
    # loc='upper left')
    ##
    # lgd = plt.figlegend(bbox_to_anchor=(-0.40, 0.9), bbox_transform=ax_histy.transAxes,
    # loc='upper left')
    ##

    # lgd = ax_scatter.legend(bbox_to_anchor=(-0.40, 0.9), bbox_transform=ax_histy.transAxes,
    #                         loc='upper left')

    # adjust the subplot params when it is called
    # plt.tight_layout()

    # plt.show()
    # if args.hists:
    #     ax_histx.invert_xaxis()

    # be carefull that th efigure box does not cut the legend :
    # https://stackoverflow.com/q/10101700
    fig.savefig(
        args.pathout,
        dpi=600,
        bbox_extra_artists=(
            title, lgd,),
        bbox_inches='tight'
    )

    print("# node max ", max(x_coord))

    print("correl Pierson'r", scipy.stats.pearsonr(x_coord, y_coord))
    print("correl Sperman'", scipy.stats.spearmanr(x_coord, y_coord))
    print("correl Kendall'tau", scipy.stats.kendalltau(x_coord, y_coord))

    print("the end")
