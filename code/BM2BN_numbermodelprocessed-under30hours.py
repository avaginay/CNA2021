

import argparse
import pathlib
import re
import sys
import textwrap

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

try:
    sys.path.insert(1, 'code/')
except ImportError:
    print("is it the correct working directory ?")
    exit(1)
    # %pwd


if __name__ == "__main__":

    # %% help message
    parser = argparse.ArgumentParser(
        description="""
        """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--benchmarkfolder',
                        type=pathlib.Path,
                        required=True,
                        help=textwrap.dedent('''\
                        Folder where the benchmark files are stored.
                        ''')
                        )

    # %pwd
    # benchmarkfolder = pathlib.Path(
    #    "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/xp-runningexampleCMSB2021/benchmarks/")
    # benchmarkfolder = pathlib.Path(
    #    "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/results-20210316_CMSB3021_merge_1to10/benchmarks/")
    benchmarkfolder = pathlib.Path(
        "/home/nanis/Documents/TRAVAIL/THESE/PROJETS/CMSB2020/experiments/CMSB2021-results_merge-all_1to10/benchmarks/")

    identificationmethods = ["caspots", "our"]  # ["caspots", "our"]

    # ["caspots", "our"]:

    benchmark_data = []
    for identificationmethod in identificationmethods:
        print(identificationmethod)

        # --------------------------------------------------------------------------
        # Determine the list of benchmark files to process

        # process all the benchmark files of a given identification method.
        benchmark_files = list(benchmarkfolder.glob(
            f"*.identification-{identificationmethod}.benchmark.txt"))
        benchmark_files

        # --------------------------------------------------------------------------
        COLUMNS = ["s", "h:m:s", "max_rss", "max_vms", "max_uss",
                   "max_pss", "io_in", "io_out", "mean_load", "cpu_time"]

        for filename in benchmark_files:
            print(filename)
            df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
            df["benchmark_file"] = filename
            df["identificationmethod"] = identificationmethod
            # print(df)
            benchmark_data.append(df)

    benchmark_df = pd.concat(
        benchmark_data, axis=0, ignore_index=True, sort=True)
    # benchmark_df = pd.DataFrame(benchmark_data, columns=)
    benchmark_df.head()
    benchmark_df.shape

    thirtyhours_inseconds = 30 * 60 * 60

    tooklessthan30h = benchmark_df.loc[benchmark_df['cpu_time']
                                       <= thirtyhours_inseconds]
    tooklessthan30h.shape

    sbml_id = set()
    for filename in benchmark_df.benchmark_file:
        # print(filename)
        sbml_id.add(re.findall('BIOMD[0-9]+', str(filename))[0])
    len(sbml_id)

    tooklessthan30h_sbmlid = set()
    for filename in tooklessthan30h.benchmark_file:
        # print(filename)
        tooklessthan30h_sbmlid.add(re.findall('BIOMD[0-9]+', str(filename))[0])

    len(tooklessthan30h_sbmlid)
