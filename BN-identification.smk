import itertools
import pathlib
import sys

import pandas as pd

try:
    sys.path.insert(1, 'code/')
    import tshandler
except ImportError:
    print("is it the correct working directory ?")
    # %pwd


for key in (
        "folder_pkn",
        "folder_ts",
        "folder_lp",
        "folder_bn",
        "folder_heatmapsCompflictingTimeSteps",
        "folder_benchmarks"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])


gW_SYSTEM_id_ts, = glob_wildcards(
    config['folder_ts'] / "{SYSTEM_id}_ts.csv")
gW_SYSTEM_id_pkn, = glob_wildcards(
    config['folder_pkn'] / "{SYSTEM_id}_pkn.sif")
gW_SYSTEM_id = gW_SYSTEM_id_ts + gW_SYSTEM_id_pkn


# about config["optimization"]
# optimization = "mae" -> minimize MAE between :
#   - obs ts (stretched between -100 and 100)
#   - binarized ts (-100 representing 0 and 100 representing 1)
# optimization = "unexplainedbunary" -> minimize the number of timestep where
#   the parents configuration observed at time T-1 as been observed to be the cube
#   but the child has not been activated at time T.
#   Note that ASP will work only on the binarized ts, potentially deduplicated is no inconsistency has been spotted


def aggregate_bnetfiles(wildcards):
    # input function for the rule aggregate

    bnet_folders_fromcheckpoints = []

    if "our" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.lp2bn.get(
            SYSTEM_id=SYSTEM_id).output for SYSTEM_id in gW_SYSTEM_id)

    if "reveal" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.bnet_reveal.get(
            SYSTEM_id=SYSTEM_id).output for SYSTEM_id in gW_SYSTEM_id)

    if "bestfit" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.bnet_bestfit.get(
            SYSTEM_id=SYSTEM_id).output for SYSTEM_id in gW_SYSTEM_id)

    if "caspots" in config["identification_methods"]:
        bnet_folders_fromcheckpoints.extend(checkpoints.bnet_caspots.get(
            BIOMD_id=BIOMD_id).output for BIOMD_id in gW_BIOMD_id)

    # bnet_folders_fromcheckpoints is a list of lists containing one element : the folder that was outputed by the checkpoint
    bnet_files = [list(pathlib.Path(bn_folder).glob("*.bnet"))
                  for bn_folder in itertools.chain.from_iterable(bnet_folders_fromcheckpoints)]
    bnet_files_flatList = list(itertools.chain.from_iterable(bnet_files))

    bnet_files_flatList = [str(path) for path in bnet_files_flatList]
    print(bnet_files_flatList)
    return bnet_files_flatList


rule all_BNidentification:
    input:
        # One lp file per node taken into account in the sbml
        # Either do (does not work because following sm error "Wildcards in input files cannot be determined from output file"):
        # [config["folder_lp"] / "{SYSTEM_id}" / "knowledge_{node}.lp" for SYSTEM_id, node in get_SYSTEM_id2nodes_mapping(gW_SYSTEM_id, get_nodesTaken(gW_SYSTEM_id))],
        # Or with the expand with a custom function with map the SYSTEM_id to its nodes (does not work):
        # expand(
        #    config['folder_lp'] / "{SYSTEM_id}" /"knowledge_ts-{node}.lp",
        #    get_SYSTEM_id2nodes_mapping,
        #    SYSTEM_id=gW_SYSTEM_id,
        #    node = get_nodesTaken(gW_SYSTEM_id)
        # ),
        # Or just call other lp files and lp files for nodes will be generating along.
        # Problem, since they are defined dynamically, they would be regenerated everytime the pipeline is ran.
        # other lp files :
        # expand(config['folder_lp'] / "{SYSTEM_id}" /
        #    "knowledge_ts-stretched.lp", SYSTEM_id=gW_SYSTEM_id) if config["optimization"] == "mae" else "",
        # expand(config['folder_lp'] / "{SYSTEM_id}" /
        #    "knowledge_ts-binarized.lp", SYSTEM_id=gW_SYSTEM_id) if config["optimization"] == "mae" else "",
        # Just call the task.done file, and all the other lp files will be generated along :
        # expand(config["folder_lp"] / "{SYSTEM_id}" /
        #        "generate_lp.done", SYSTEM_id=gW_SYSTEM_id),
        # bnet results : At least 1 bn will be generated : 0.bnet
        # folder are not taken into account in input rule.
        # but we can gather outputs of checkpont rule
        # with a re-evaluation of the chekpoint and access to its output with this function
        aggregate_bnetfiles,
        # dynamic(
        #    config["folder_bn"] / "{SYSTEM_id}" / "{BN_id}.bnet"),
        # expand(
        #    config["folder_bn"] / "{SYSTEM_id}" / "{BN_id}_stg-synchronous_focusedbts.sif", BN_id=gW.BN_id),
        # expand(
        #    config["folder_bn"] / "{SYSTEM_id}" / "{BN_id}_stg-synchronous_focusedbts.png", BN_id=gW.BN_id),
        # dynamic(
        #    config["folder_bn"] / "{SYSTEM_id}" / "{BN_id}_stg-synchronous.png")


gW_SYSTEM_id_generatedbnet, gW_identification_method, gW_BNid_generatedbnet = glob_wildcards(
    config["folder_bn"] / "{SYSTEM_id}" / "{identification_method}" / "{BN_id}.bnet")
print("generated bnet:")
print(gW_SYSTEM_id_generatedbnet)
print(gW_identification_method)
print(gW_BNid_generatedbnet)


rule all_lp:
    input:
        expand(config["folder_lp"] / "{SYSTEM_id}" /
               "generate_lp.done", SYSTEM_id=gW_SYSTEM_id)

rule all_binarizedts:
    input:
        expand(config['folder_ts'] /
               "{SYSTEM_id}_binarizedtsts.csv", SYSTEM_id=gW_SYSTEM_id)


rule tscsv2tsMIDAS:
    input:
        config['folder_ts'] / "{SYSTEM_id}_ts.csv"
    output:
        config['folder_ts'] / "{SYSTEM_id}_ts_MIDAS.csv"
    shell:
        "python3 code/tscsv2MIDAS.py --tscsv {input} --outputfilepath {output}"


rule ts2stretchedts:
    input:
        config['folder_ts'] / "{SYSTEM_id}_ts.csv"
    output:
        config['folder_ts'] / "{SYSTEM_id}_stretchedts.csv"
    shell:
        "python3 code/ts2stretchedts.py --tsfilepath {input} --outfilepath {output} --globally {config[globalbinarizationthreshold]}"


rule stretchedts2binarizedts:
    input:
        config['folder_ts'] / "{SYSTEM_id}_stretchedts.csv"
    output:
        config['folder_ts'] / "{SYSTEM_id}_binarizedtsts.csv"
    shell:
        "python3 code/stretchedts2binarizedts.py --stsfilepath {input} --outfilepath {output}"


# Takes an sbml file + the associated res-simu.csv file + an output directory path
# Outputs the pkn and observation as ASP for each agent
rule generate_lp:
    input:
        sif = config['folder_pkn'] / "{SYSTEM_id}.sif",
        ts = config['folder_ts'] / "{SYSTEM_id}_ts.csv"
    params:
        outdirpath = lambda wildcards: config['folder_lp'] / \
            wildcards.SYSTEM_id
    output:
        # - lp file are temp files
        #   thus will be deleted after all rules that use it as an input are completed
        # - we do not know in advance how many lp files their is (one per node)
        #   if we use the `dynamic` keyword, all the output of the rule have to be dynamic.
        #   Thus, it is not possible to use the following :
        #   -----
        #   temp(dynamic(config["folder_lp"] /
        #              "{SYSTEM_id}" / "knowledge_{node}.lp")),
        #   temp(config['folder_lp'] / "{SYSTEM_id}" /
        #      "knowledge_ts-stretched.lp" if config["optimization"] == "mae" else ""),
        #   temp(config['folder_lp'] / "{SYSTEM_id}" /
        #      "knowledge_ts-binarized.lp" if config["optimization"] == "mae" else ""),
        #   touch(config["folder_lp"] / "{SYSTEM_id}" / "task.done")
        #   -----
        #   Instead :
        #   Either just add a dynamic output
        # temp(dynamic(config["folder_lp"] / "{SYSTEM_id}" / "knowledge_{filename}.lp"))
        #   OR : List them all.
        #   But in this case, the problem is that all the files need to have the same wildcards.
        #   which is not the case, since lp file for node would also have the wilcard "node"
        #
        # - One lp file per node taken into account in the sbml
        # Difficult since output can not expand from params
        # nor use a lambda function.
        # so either do (does not work because all the output file do not have the same wildcards):
        # [config["folder_lp"] / "{{SYSTEM_id}}" / "knowledge_{{node}}.lp" for SYSTEM_id, nodes in get_SYSTEM_id2nodes_mapping(gW_SYSTEM_id, get_nodesTaken(gW_SYSTEM_id))],
        # Or with the expand with a custom function with map the SYSTEM_id to its nodes (does not work):
        # expand(
        #    config['folder_lp'] / "{SYSTEM_id}" /"knowledge_ts-{node}.lp",
        #    get_SYSTEM_id2nodes_mapping,
        #    SYSTEM_id=gW_SYSTEM_id,
        #    node = get_nodesTaken(gW_SYSTEM_id)
        # ),
        # Following does not work since only input can be specified as functions :
        # lambda wildcards: [
        #    f"{config['folder_lp']}/{wildcards.SYSTEM_id}/knowledge_{node}.lp" for node in get_nodesTaken([wildcards.SYSTEM_id], safe=True)[0]],
        # - Other lp files
        temp(config['folder_lp'] / "{SYSTEM_id}" /
             "knowledge_ts-stretched.lp" if config["optimization"] == "mae" else ""),
        temp(config['folder_lp'] / "{SYSTEM_id}" /
             "knowledge_ts-binarized.lp" if config["optimization"] == "mae" else ""),
        #
        # Problem if the files here are define with dynamic, they will be regenerated everytime the pipeline is ran.
        # Instead : touch one file, all the other lp file will be generated along.
        touch(config["folder_lp"] / "{SYSTEM_id}" / "generate_lp.done")
    shell:
        "python3 code/generate_lp.py --sif_fname {input.sif} --res_simu_fname {input.ts} --out_dirpath {params.outdirpath} --optimization_option {config[optimization]} --globally {config[globalbinarizationthreshold]}"


def get_nodesTaken(SYSTEM_ids, safe=False):
    """
    Argument:
    ---------
    SYSTEM_ids : list of SYSTEM_ids

    Returns:
    --------
    list of list of nodes taken, for each SYSTEM_id in SYSTEM_ids
    the list of nodes is retrived from the columns of the ts associated to SYSTEM_ids
    """
    nodesTaken_perSYSTEMid = []
    for SYSTEM_id in SYSTEM_ids:
        print(SYSTEM_id)
        ts = tshandler.tsfile2pddf(
            config["folder_ts"] / f"{SYSTEM_id}_ts.csv")
        nodesTaken = ts.columns.values
        nodesTaken_perSYSTEMid.append(nodesTaken)
    print(nodesTaken_perSYSTEMid)
    return nodesTaken_perSYSTEMid


rule lp2bn:
    input:
        # The following does not work since I finally did not put lp files for nodes in the output list of the rule sbml
        # Thus Snakemake complains there is input file missing.
        # lpknowledge_agents = lambda wildcards: [
        #    f"{config['folder_lp']}/{wildcards.SYSTEM_id}/knowledge_{node}.lp" for node in get_nodesTaken([wildcards.SYSTEM_id], safe=True)[0]],
        # Instead, use param and ensure lp for nodes have been generated using the following
        config["folder_lp"] / "{SYSTEM_id}" / "generate_lp.done",
        # only on SYSTEM_id provided, so use the index 0 to access list of nodes
        lpknowledge_stretchedts = config['folder_lp'] / "{SYSTEM_id}" /
        "knowledge_ts-stretched.lp" if config["optimization"] == "mae" else "",
        lpknowledge_binarizedts = config['folder_lp'] / "{SYSTEM_id}" /
        "knowledge_ts-binarized.lp" if config["optimization"] == "mae" else ""
    params:
        lpknowledge_agents = lambda wildcards: [
            f"{config['folder_lp']}/{wildcards.SYSTEM_id}/knowledge_{node}.lp" for node in get_nodesTaken([wildcards.SYSTEM_id], safe=True)[0]],
        # outputdirpath = f"{config['folder_bn']}/{{SYSTEM_id}}",
        heatmapdirpath = f"{config['folder_heatmapsCompflictingTimeSteps']}/{{SYSTEM_id}}",
        folder_out = lambda wildcards: config["folder_bn"] / \
            f"{wildcards.SYSTEM_id}" / "our"
    output:
        # identification_method = "our"
        # this rule use to be a checkpoint having the following directory as output:
        # directory(config["folder_bn"] / "{SYSTEM_id}" / "our"),
        # but because the checkpoints of an aggregate depending on several chekpoints were not run in parallele,
        # https://github.com/snakemake/snakemake/issues/439
        # it now uses the trick of touching a file:
        touch(config["folder_bn"] / "{SYSTEM_id}" /
              "our" / "identification.done")
        # config["folder_bn"] / "{SYSTEM_id}" / "{BN_id}.bnet",
        # dynamic( config["folder_bn"] / "{SYSTEM_id}" / "{BN_id}.conflicting_timestep")
        # cannot do this :
        # touch("task.done")
        # because currently cannot mix non-dynamic and dynamic outputs in the same rule
        # https://stackoverflow.com/a/53175631
        # (experimental support for dynamic files)
    threads: config["clingo_nbthreads"]
    benchmark: config["folder_benchmarks"] / \
        "{SYSTEM_id}.identification-our.benchmark.txt"
    shell:
        # From the documentaation :
        # http://snakemake.readthedocs.io/en/stable/snakefiles/configuration.html#standard-configuration
        #   > For adding config placeholders into a shell command,
        #   > Python string formatting syntax requires you to
        #   > leave out the quotes around the key name
        # Same for any dict defined in the Snakemake file.
        """
        python3 code/clingoidentification.py\
        --nbcores {threads}\
        --stretchedtsfilepath {input.lpknowledge_stretchedts}\
        --binarizedtsfilepath {input.lpknowledge_binarizedts}\
        --outputdirpath {params.folder_out}\
        --optimization {config[optimization]}\
        --plotsdirpath {params.heatmapdirpath}\
        --nbmaxgeneratedBNs {config[nbmaxgeneratedBNs]}\
        {params.lpknowledge_agents} # lpfiles
        """


# BNET_IDS, = glob_wildcards(config["folder_bn"] / "{BN_id}.bnet")
