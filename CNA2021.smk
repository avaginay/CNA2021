
import os
import pathlib
import shutil

include: "sbml2data.smk"
# include: "data-info.smk"
include: "BN-identification.smk"
include: "BN-analysis.smk"
# include: "plots.smk"


for key in (
    "folder_pkn",
    "folder_ts",
    "folder_xp",
    "folder_bn",
    "folder_results",
    "RUNNINGEXAMPLES_folder_bn"
):
    print("->" + key)
    print(config[key])
    config[key] = pathlib.Path(config[key])



rule sbmlList_Biomodels_curated:
    # all the sbml from Biomodels/curated
    output:
        "sbmlList/sbmlList_Biomodels_curated"
    shell:
        "ls data/BioModels/BioModels_Database-r31_pub-sbml_files/curated/BIOMD*.xml >  {output}"

rule htmlreport_stats_sbmlList_Biomodels_curated:
    input:
        "sbmlList/sbmlList_Biomodels_curated"
    output:
        directory("htmlreports/htmlreport_sbmlList_Biomodels_curated_stats")
    shell:
        "python3 code/htmlreport_stats-sbmlList.py --sbmlList {input} --outfolderpath {output}"

rule htmlreport_stats_sbmlList_Biomodels_curated_runsimuOK_pknOK:
    input:
        "sbmlList/sbmlList_Biomodels_curated_withinfots_runsimuOK_getpknOK"
    output:
        directory(
            "htmlreports/sbmlList_Biomodels_curated_withinfots_runsimuOK_getpknOK_stats"
        )
    shell:
        "python3 code/htmlreport_stats-sbmlList.py --sbmlList {input} --outfolderpath {output}"

rule htmlreport_curation_images:
    input:
        "sbmlList/sbmlList_Biomodels_curated"
    output:
        directory(
            "htmlreports/htmlreport_sbmlList_Biomodels_curated_curationimages")
    shell:
        """
        python3 code/htmlreport_length-curated-simulation_Biomodels.py --sbmlList {input} --outfolderpath {output}
        """

rule sbmlList_Biomodels_curated_withinfots:
    # sbml from Bomodels/curated that I found having a curation image looking like a ts
    input:
        sbmlList = "sbmlList/sbmlList_Biomodels_curated",
        curationTable = "table_curatedBIOMD_time-curation-ts.ods",
    output:
        "sbmlList/sbmlList_Biomodels_curated_withinfots"
    shell:
        "python3 code/handle_curationTable.py --sbmlList {input.sbmlList} --curationTable {input.curationTable} --outputsbmlList {output} --property 'sbmlwithcurationts'"


rule sbmlList_Biomodels_curated_withinfotscomplete:
    # sbml from Bomodels/curated that I found having a curation image looking like a ts
    # and from which i could extract timestart, timestop and unit
    input:
        sbmlList = "sbmlList/sbmlList_Biomodels_curated",
        curationTable = "table_curatedBIOMD_time-curation-ts.ods",
    output:
        "sbmlList/sbmlList_Biomodels_curated_withinfotscomplete"
    shell:
        "python3 code/handle_curationTable.py --sbmlList {input.sbmlList} --curationTable {input.curationTable} --outputsbmlList {output}  --property 'sbmlwithcurationts_complete' "

# rule run_simulation_withcurationinfots:
#     input:
#         sbmlList = "sbmlList/sbmlList_Biomodels_curated_withinfots",
#         curationTable = "table_curatedBIOMD_time-curation-ts.ods",
#     output:
#         config["folder_ts"] / "{SBML_id}_ts.csv"
#     shell:
#         # "python3 code/htmlreport_compare-ts-with-curatedts.py --xprespath {config[xp_name]}"


rule htmlreport_tswithinfoVStscurated:
    input:
        "sbmlList/sbmlList_Biomodels_curated_withinfots"
    output:
        directory(
            "htmlreports/htmlreport_sbmlList_Biomodels_curated_withinfots_tsVScurationimages")
    shell:
        "python3 code/htmlreport_compare-ts-with-curatedts.py --sbmlList {input} --outfolderpath {output}"


def aggregate_bnetfiles(wildcards):
    # input function for the rule aggregate
    print(wildcards)
    print("aggregate XXX")
    identificationdone = []

    if "our" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}"
                                   / "our" / "identification.done" for SYSTEM_id in gW_SBML_id])

    if "reveal" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}"
                                   / "reveal" / "identification.done" for SYSTEM_id in gW_SBML_id])

    if "bestfit" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}"
                                   / "bestfit" / "identification.done" for SYSTEM_id in gW_SBML_id])

    if "caspots" in config["identification_methods"]:
        identificationdone.extend([config["folder_bn"] / f"{SYSTEM_id}"
                                   / "caspots" / "identification.done" for SYSTEM_id in gW_SBML_id])

    # list of all the "identification.done" files" created by the identification rules
    return identificationdone


rule all_BNsidentification:
    input:
        aggregate_bnetfiles


###############################################################################
########## AGGREGATION RESULTS ################################################
###############################################################################


rule all_aggregationresults:
    input:
        config['folder_results'] / "number_generatedBNs_table.csv",
        config['folder_results'] / "aggregation_resultats_table_mixed.csv",
        # no jitter
        config['folder_results'] / \
            "aggregation_results_jitterx0jittery0.png",
        # jitter x y of 0.1
        config['folder_results'] / \
            "aggregation_results_jitterx0.1jittery0.1.png",
        config['folder_results'] / \
            "aggregation_results_jitterx0.1jittery0.1_CNA2021.png",
        # cumulative plot of runtime :


rule cumulative_plot_runtime:
    params:
        benchmarkfolder = config["folder_benchmarks"]
    output:
        pathout = "20210331_CNA2021_cumulativeplot-cputime-hours.png"
    shell:
        """
        python3 code/benchmark_cumulative-plots.py --benchmarkfolder {params.benchmarkfolder} --pathout {output}
        """

rule boxplot_runtime:
    params:
        benchmarkfolder = config["folder_benchmarks"]
    output:
        # pathout = "20210331_CMSB2021_boxplot-cputime-hours.png"
        pathout = "20210331_CNA2021_cumulativeplotandboxplot-cputime-hours.png"
    shell:
        # python3 code/benchmark_boxplot.py --benchmarkfolder {params.benchmarkfolder} --pathout {output}
        """
        python3 code/benchmark_cumulative-plots-and-boxplots.py --benchmarkfolder {params.benchmarkfolder} --pathout {output}
        """

rule all_aggregation_results_image:
    input:
        config['folder_results'] /
        "aggregation_results_jitterx0jittery0.png",
        config['folder_results'] /
        "aggregation_results_jitterx0.1jittery0.1.png",
        config['folder_results'] / \
            "aggregation_results_jitterx0.1jittery0.1_CNA2021.png",


rule aggregation_results_image:
    input:
        config['folder_results'] / "aggregation_resultats_table_mixed.csv"
    output:
        config['folder_results'] / \
            "aggregation_results_jitterx{jitter_x}jittery{jitter_y}.png",
        config['folder_results'] / \
            "aggregation_results_jitterx{jitter_x}jittery{jitter_y}_CNA2021.png"
    shell:
        # Note :
        # the argument "pattern" is a relicat from the code of OLA21.
        """
        # python3 code/aggregation_results_image.py --aggregation_results_table {input} --pattern 'BIOMD' --jitter_x {wildcards.jitter_x} --jitter_y {wildcards.jitter_y} --hists True --identificationmethods our --plotX median --pathout {output}
        python3 code/aggregation_results_image_CNA2021.py --aggregation_results_table {input} --pattern 'BIOMD' --jitter_x {wildcards.jitter_x} --jitter_y {wildcards.jitter_y} --hists True --identificationmethods our --plotY median --pathout {output}
        """

rule aggregation_results_table:
    input:
        # all the BNs computed with all the methods for all the systems
        bns = aggregate_bnetfiles,
        # all coverage files on the wanted SYSTEM :
        # Note : in the lambda functions, the wildcards are in f-strings.
        cov_reveal = [str(p) for SYSTEM_id in gW_SBML_id for p in (
            config["folder_bn"] / f"{SYSTEM_id}" / "reveal").glob(f"*_coverage_stgVSts_mixed.csv")],
        cov_bestfit = [str(p) for SYSTEM_id in gW_SBML_id for p in (
            config["folder_bn"] / f"{SYSTEM_id}" / "bestfit").glob(f"*_coverage_stgVSts_mixed.csv")],
        cov_caspots = [str(p) for SYSTEM_id in gW_SBML_id for p in (
            config["folder_bn"] / f"{SYSTEM_id}" / "caspots").glob(f"*_coverage_stgVSts_mixed.csv")],
        # Adding the following causes an error "File/directory is a child to another output" :
        # cov_our = [str(p) for SYSTEM_id in SYSTEMS_ids for p in (
        #    config["folder_bn"] / f"{SYSTEM_id}" / "our").glob(f"*_coverage_stgVSts_mixed.csv")]
    output:
        # a table with systems in line, method in col, and values are number of BN gererated.
        # for REVEAL and BestFit, also write the number of BN before PKN filter.
        config['folder_results'] / \
            "aggregation_resultats_table_mixed.csv"
    shell:
        "python3 code/aggregation_results_table.py --benchmakspath {config[folder_benchmarks]} --pknpath {config[folder_pkn]} --bnpath {config[folder_bn]} --updatescheme mixed --pathout {output}"


rule all_number_generatedBNs:
    input:
        config['folder_results'] / "number_generatedBNs_table.csv"


rule number_generatedBNs_table:
    input:
        # all the BNs computed with all the methods for all the systems
        aggregate_bnetfiles
    output:
        # a table with systems in ligne, method in col, and values are number of BN gererated.
        # for REVEAL and BestFit, also write the number of BN before PKN filter.
        config['folder_results'] / "number_generatedBNs_table.csv"
    shell:
        # --updatescheme {config[coverage_updatescheme]}
        "python3 code/number_generatedBNs_table.py --bnpath {config[folder_bn]} --pathout {output}"
